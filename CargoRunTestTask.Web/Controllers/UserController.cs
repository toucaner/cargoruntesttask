﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CargoRunTestTask.Bll.User;
using CargoRunTestTask.Bll.UserRole;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Web.Models;
using CargoRunTestTask.Web.Models.Account;
using Microsoft.AspNetCore.Authorization;

namespace CargoRunTestTask.Web.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        #region Fields

        private readonly IUserService _userService;

        private readonly IUserRoleService _userRoleService;

        #endregion

        #region Constructor

        public UserController(IUserService userService, IUserRoleService userRoleService)
        {
            _userService = userService;
            _userRoleService = userRoleService;
        }

        #endregion

        #region Methods

        public async Task<IActionResult> Index(string filter, int page = 1, int pageSize = 5)
        {
            var viewModel = new ListViewModel<UserDto>();
            var userListResult = await _userService.GetUserList(filter, page, pageSize);
            var countResult = await _userService.GetUserCount(filter);

            if (!userListResult.Success)
                ModelState.AddModelError("", userListResult.Error.Message);

            if (!countResult.Success)
                ModelState.AddModelError("", countResult.Error.Message);

            if (userListResult.Success && countResult.Success)
            {
                viewModel = new ListViewModel<UserDto>
                {
                    PageViewModel = new PageViewModel(countResult.Value, page, pageSize),
                    FilterViewModel = new FilterViewModel(filter),
                    Items = userListResult.Value
                };
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Register()
        {
            var roleResult = await _userRoleService.GetRoles();
            if (!roleResult.Success)
                ModelState.AddModelError("", roleResult.Error.Message);

            ViewBag.Roles = roleResult.Value;

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int userId)
        {
            var result = await _userService.DeleteUser(userId);
            if (!result.Success)
                ModelState.AddModelError("", result.Error.Message);

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int userId)
        {
            var roleResult = await _userRoleService.GetRoles();
            if (!roleResult.Success)
                ModelState.AddModelError("", roleResult.Error.Message);

            ViewBag.Roles = roleResult.Value;
            var userResult = await _userService.GetUser(userId);
            if (!userResult.Success)
                ModelState.AddModelError("", userResult.Error.Message);

            return View(userResult.Value);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserDto model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _userService.FindUser(model.Email);
                if (!userResult.Success)
                {
                    ModelState.AddModelError("", userResult.Error.Message);
                }
                else
                {
                    if (userResult.Value != null)
                    {
                        var updateResult = await _userService.UpdateUser(
                            new UserDto
                            {
                                Id = model.Id,
                                Email = model.Email,
                                Password = model.Password,
                                RoleId = model.RoleId
                            });

                        if (!updateResult.Success)
                        {
                            ModelState.AddModelError("", userResult.Error.Message);
                        }
                        else
                        {
                            return RedirectToAction("Index", "User");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Ошибка редактирования пользователя");
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _userService.FindUser(model.Email);
                if (!userResult.Success)
                {
                    ModelState.AddModelError("", userResult.Error.Message);
                }
                else
                {
                    if (userResult.Value == null)
                    {
                        var createResult = await _userService.CreateUser(
                            new UserDto
                            {
                                Email = model.Email,
                                Password = model.Password,
                                RoleId = model.RoleId
                            });

                        if (!createResult.Success)
                        {
                            ModelState.AddModelError("", userResult.Error.Message);
                        }
                        else
                        {
                            return RedirectToAction("Index", "User");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Ошибка регистрации пользователя");
                    }
                }
            }

            return View(model);
        }
    }

    #endregion
}
