﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CargoRunTestTask.Bll.User;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Web.Models.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CargoRunTestTask.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Methods

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _userService.FindUser(model.Email, model.Password);
                if (!userResult.Success)
                {
                    ModelState.AddModelError("", userResult.Error.Message);
                }
                else
                {
                    if (userResult.Value != null)
                    {
                        await Authenticate(userResult.Value);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return View(model);
        }

        private async Task Authenticate(UserDto user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.RoleName)
            };

            var claimsIdentity = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        } 
        
        #endregion
    }
}