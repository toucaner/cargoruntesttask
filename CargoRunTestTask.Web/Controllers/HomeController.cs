﻿using CargoRunTestTask.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CargoRunTestTask.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region Methods
        
        public IActionResult Index()
        {
            var role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;
            return role switch
            {
                "Administrator" => RedirectToAction("Index", "User"),
                "Librarian" => RedirectToAction("Login", "Account"),
                "Client" => RedirectToAction("Login", "Account"),
                _ => RedirectToAction("Login", "Account")
            };
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        } 
        
        #endregion
    }
}
