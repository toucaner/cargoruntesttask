﻿using System.Collections.Generic;
using CargoRunTestTask.Core.Dto;

namespace CargoRunTestTask.Web.Models
{
    public class ListViewModel<TDto> where TDto : DtoBase
    {
        #region Properties

        public IEnumerable<TDto> Items { get; set; }
        
        public PageViewModel PageViewModel { get; set; }
        
        public FilterViewModel FilterViewModel { get; set; }

        #endregion
    }
}