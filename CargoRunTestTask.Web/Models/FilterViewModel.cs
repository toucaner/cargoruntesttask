﻿namespace CargoRunTestTask.Web.Models
{
    public class FilterViewModel
    {
        #region Properties

        public string SelectedFilter { get; }

        #endregion
        
        #region Constructor

        public FilterViewModel(string filter)
        {
            SelectedFilter = filter;
        } 
        
        #endregion
    }
}