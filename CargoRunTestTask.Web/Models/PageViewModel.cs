﻿using System;

namespace CargoRunTestTask.Web.Models
{
    public class PageViewModel
    {
        #region Properties
        
        public int PageNumber { get; }

        public int TotalPages { get; }

        public bool HasPreviousPage => (PageNumber > 1);

        public bool HasNextPage => (PageNumber < TotalPages);

        #endregion

        #region Constructor
        
        public PageViewModel(int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        } 
        
        #endregion
    }
}