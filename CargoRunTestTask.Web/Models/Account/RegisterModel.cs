﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Web.Models.Account
{
    public class RegisterModel
    {
        #region Properties
        
        [Required(ErrorMessage = "Не указан email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указана роль")]
        public int RoleId { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; } 
        
        #endregion
    }
}