﻿using AutoMapper;
using CargoRunTestTask.Core.Domain;
using CargoRunTestTask.Core.Dto;

namespace CargoRunTestTask.Core.Mapping
{
    public class MappingProfile : Profile
    {
        #region Constructor

        public MappingProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Role.Name))
                .ForMember(dest => dest.RoleDisplayName, opt => opt.MapFrom(src => src.Role.DisplayName));
            CreateMap<UserDto, User>();
            CreateMap<UserRole, UserRoleDto>().ReverseMap();
        }

        #endregion
    }
}