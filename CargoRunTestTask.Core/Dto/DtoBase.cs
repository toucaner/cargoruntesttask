﻿namespace CargoRunTestTask.Core.Dto
{
    public abstract class DtoBase
    {
        #region Properties

        public long Id { get; set; }

        public bool IsDeleted { get; set; }

        #endregion
    }
}