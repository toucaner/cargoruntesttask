﻿namespace CargoRunTestTask.Core.Dto
{
    public class UserRoleDto : DtoBase
    {
        #region Properties

        public string Name { get; set; }

        public string DisplayName { get; set; }

        #endregion
    }
}