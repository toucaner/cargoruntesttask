﻿namespace CargoRunTestTask.Core.Dto
{
    public class UserDto : DtoBase
    {
        #region Properties

        public string Email { get; set; }

        public string Password { get; set; }
        
        public int RoleId { get; set; }
        
        public string RoleName { get; set; }

        public string RoleDisplayName { get; set; }

        #endregion
    }
}