﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public abstract class EntityBase
    {
        #region Properties

        [Key]
        public long Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;

        public DateTime Modified { get; set; } = DateTime.UtcNow;

        public bool IsDeleted { get; set; }

        #endregion
    }
}