﻿using System.Collections.Generic;

namespace CargoRunTestTask.Core.Domain
{
    public class UserRole : СollectionEntityBase
    {
        #region Properties

        public string DisplayName { get; set; }

        public virtual List<User> Users { get; set; }
        
        #endregion
    }
}