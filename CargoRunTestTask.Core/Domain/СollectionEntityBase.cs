﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class СollectionEntityBase : EntityBase
    {
        #region Properties

        [Required]
        public string Name { get; set; }

        #endregion
    }
}