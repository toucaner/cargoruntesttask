﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class Booking : EntityBase
    {
        #region Properties

        [Required]
        public DateTime StartDate { get; set; } = DateTime.UtcNow;

        [Required]
        public DateTime EndDate { get; set; } = DateTime.UtcNow;

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required]
        public virtual Book Book { get; set; }

        #endregion
    }
}