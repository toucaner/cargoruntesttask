﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class Сomment : EntityBase
    {
        #region Properties

        [Required]
        public string Text { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required]
        public virtual Book Book { get; set; }

        #endregion
    }
}