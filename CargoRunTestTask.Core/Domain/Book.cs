﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class Book : СollectionEntityBase
    {
        #region Properties

        [Required]
        public virtual Author Author { get; set; }

        [Required]
        public virtual Genre Genre { get; set; }

        [Required]
        public virtual Publisher Publisher { get; set; }

        #endregion
    }
}