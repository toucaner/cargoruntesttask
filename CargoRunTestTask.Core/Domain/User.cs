﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class User : EntityBase
    {
        #region Properties

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public virtual UserRole Role { get; set; }

        #endregion
    }
}