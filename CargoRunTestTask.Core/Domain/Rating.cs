﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Core.Domain
{
    public class Rating : EntityBase
    {
        #region Properties

        [Required]
        [Range(1, 5)]
        public int Value { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required]
        public virtual Book Book { get; set; }

        #endregion
    }
}