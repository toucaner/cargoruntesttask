﻿using System;
using CargoRunTestTask.Utils.Enums;
using CargoRunTestTask.Utils.Helpers;

namespace CargoRunTestTask.Utils.Common
{
    public class Error : IEquatable<Error>
    {
        #region Properties
        
        public string Message { get; protected set; }

        public int Code { get; protected set; } 
        
        #endregion

        #region Constructors

        public Error(string message, int code = -1)
        {
            Message = message;
            Code = code;
        }

        public Error(ErrorCode code)
        {
            Message = code.DisplayName();
            Code = (int)code;
        }

        #endregion

        #region Methods

        public bool Equals(Error other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Code == other.Code;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return ((Error) obj).Code == Code;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Code.GetHashCode();
        }

        #endregion
    }
}