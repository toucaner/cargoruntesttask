﻿using System.ComponentModel.DataAnnotations;

namespace CargoRunTestTask.Utils.Enums
{
    public enum ErrorCode
    {
        /// <summary>
        /// Ошибка выполнения операции
        /// </summary>
        [Display(Name = "Ошибка выполнения операции")]
        OperationExecutionError = 1000,
        /// <summary>
        /// Пользователь не найден
        /// </summary>
        [Display(Name = "Пользователь не найден")]
        UserNotFound = 1100
    }
}