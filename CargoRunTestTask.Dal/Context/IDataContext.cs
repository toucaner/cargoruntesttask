﻿using System.Threading.Tasks;
using CargoRunTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace CargoRunTestTask.Dal.Context
{
    public interface IDataContext
    {
        #region Methods

        DbSet<TEntity> Set<TEntity>() where TEntity : EntityBase;

        Task<int> SaveChangesAsync();

        #endregion
    }
}