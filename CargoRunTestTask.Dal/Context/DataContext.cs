﻿using System;
using System.Threading.Tasks;
using CargoRunTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CargoRunTestTask.Dal.Context
{
    public class DataContext : DbContext, IDataContext
    {
        #region Constructors

        public DataContext()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Properties

        public DbSet<User> Users { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Book> Books { get; set; }

        public DbSet<BookToUser> BookToUsers { get; set; }

        public DbSet<Genre> Genries { get; set; }

        public DbSet<Publisher> Publishers { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        public DbSet<Сomment> Сomments { get; set; }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
                .Build();

            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Error);
        }

        public new DbSet<T> Set<T>() where T : EntityBase => base.Set<T>();

        public new EntityEntry<T> Entry<T>(T entity) where T : EntityBase => base.Entry(entity);

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        #endregion
    }
}