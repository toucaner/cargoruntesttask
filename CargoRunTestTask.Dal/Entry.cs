﻿using CargoRunTestTask.Dal.Context;
using CargoRunTestTask.Dal.Repository.User;
using CargoRunTestTask.Dal.Repository.UserRole;
using Microsoft.Extensions.DependencyInjection;

namespace CargoRunTestTask.Dal
{
    public static class Entry
    {
        #region Methods

        public static IServiceCollection AddDal(this IServiceCollection services)
        {
            services
                .AddSingleton(typeof(IDataContext), typeof(DataContext))
                .AddScoped(typeof(IUserRepository), typeof(UserRepository))
                .AddScoped(typeof(IUserRoleRepository), typeof(UserRoleRepository));

            return services;
        }

        #endregion
    }
}