﻿using CargoRunTestTask.Dal.Context;

namespace CargoRunTestTask.Dal.Repository.UserRole
{
    public class UserRoleRepository : GenericRepository<Core.Domain.UserRole>, IUserRoleRepository
    {
        #region Constructor

        public UserRoleRepository(IDataContext context) : base(context)
        {
        }

        #endregion
    }
}