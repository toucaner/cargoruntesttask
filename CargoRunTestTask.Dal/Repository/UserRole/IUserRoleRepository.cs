﻿namespace CargoRunTestTask.Dal.Repository.UserRole
{
    public interface IUserRoleRepository : IGenericRepository<Core.Domain.UserRole>
    {
    }
}