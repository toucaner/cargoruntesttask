﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CargoRunTestTask.Core.Domain;

namespace CargoRunTestTask.Dal.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : EntityBase
    {
        #region Methods

        Task<TEntity> Get(long id);

        Task<IEnumerable<TEntity>> GetAll(bool withDeleted = false);

        Task<IEnumerable<TEntity>> GetAll(int page, int pageSize, bool withDeleted = false);

        Task Insert(TEntity entity);

        Task Update(TEntity entity);

        Task Delete(long id);

        Task Save();

        Task<int> Count(bool withDeleted = false);

        #endregion
    }
}