﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CargoRunTestTask.Dal.Context;
using Microsoft.EntityFrameworkCore;

namespace CargoRunTestTask.Dal.Repository.User
{
    public class UserRepository : GenericRepository<Core.Domain.User>, IUserRepository
    {
        #region Constructor

        public UserRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<Core.Domain.User> GetUser(string email)
        {
            return await Entities.FirstOrDefaultAsync(x => x.Email.Equals(email) && !x.IsDeleted);
        }

        public async Task<IEnumerable<Core.Domain.User>> GetAll(string filter, int page, int pageSize, bool withDeleted = false)
        {
            var result = GetQuery(filter, withDeleted);
            return await result.Skip((page - 1) * pageSize).Take(pageSize).ToArrayAsync();
        }

        public async Task<int> GetUserCount(string filter, bool withDeleted = false)
        {
            var result = GetQuery(filter, withDeleted);
            return await result.CountAsync();
        }

        private IQueryable<Core.Domain.User> GetQuery(string filter, bool withDeleted)
        {
            var result = Entities.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
                result = result.Where(entity => entity.Email.Contains(filter));
            if (!withDeleted)
                result = result.Where(entity => !entity.IsDeleted);
            
            return result;
        }

        #endregion
    }
}