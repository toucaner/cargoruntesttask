﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CargoRunTestTask.Dal.Repository.User
{
    public interface IUserRepository : IGenericRepository<Core.Domain.User>
    {
        #region Methods

        Task<Core.Domain.User> GetUser(string email);

        Task<IEnumerable<Core.Domain.User>> GetAll(string filter, int page, int pageSize, bool withDeleted = false);

        Task<int> GetUserCount(string filter, bool withDeleted = false);

        #endregion
    }
}