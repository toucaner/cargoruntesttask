﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CargoRunTestTask.Core.Domain;
using CargoRunTestTask.Dal.Context;
using Microsoft.EntityFrameworkCore;

namespace CargoRunTestTask.Dal.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : EntityBase
    {
        #region Constructor

        public GenericRepository(IDataContext context)
        {
            _context = context;
            Entities = _context.Set<TEntity>();
        }

        #endregion

        #region Fields

        private readonly IDataContext _context;

        protected readonly DbSet<TEntity> Entities;

        #endregion

        #region Methods

        public async Task<TEntity> Get(long id)
        {
            return await Entities.FirstOrDefaultAsync(entity => entity.Id == id && !entity.IsDeleted);
        }

        public async Task<IEnumerable<TEntity>> GetAll(bool withDeleted = false)
        {
            var result = Entities.AsQueryable();
            if (!withDeleted)
                result = result.Where(entity => !entity.IsDeleted);

            return await result.ToArrayAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAll(int page, int pageSize, bool withDeleted = false)
        {
            var result = Entities.AsQueryable();
            if (!withDeleted)
                result = result.Where(entity => !entity.IsDeleted);

            return await result.Skip((page - 1) * pageSize).Take(pageSize).ToArrayAsync();
        }

        public async Task Insert(TEntity entity)
        {
            entity.Created = DateTime.UtcNow;
            await Entities.AddAsync(entity);
            await Save();
        }

        public async Task Update(TEntity entity)
        {
            entity.Modified = DateTime.UtcNow;
            await Save();
        }

        public async Task Delete(long id)
        {
            var entity = await Entities.FirstOrDefaultAsync(x => x.Id == id);
            if (entity != null)
            {
                entity.IsDeleted = true;
                await Save();
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<int> Count(bool withDeleted = false)
        {
            var result = Entities.AsQueryable();
            if (!withDeleted)
                result = result.Where(entity => !entity.IsDeleted);

            return await result.CountAsync();
        }

        #endregion
    }
}