﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CargoRunTestTask.Dal.Migrations
{
    public partial class InsertData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Name", "DisplayName", "Created", "Modified", "IsDeleted" },
                values: new object[] { "Administrator", "Администратор", DateTime.UtcNow, DateTime.UtcNow, false });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Name", "DisplayName", "Created", "Modified", "IsDeleted" },
                values: new object[] { "Librarian", "Библиотекарь", DateTime.UtcNow, DateTime.UtcNow, false });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Name", "DisplayName", "Created", "Modified", "IsDeleted" },
                values: new object[] { "Client", "Клиент", DateTime.UtcNow, DateTime.UtcNow, false });

            // admin@admin.ru 12345
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Email", "Password", "RoleId", "Created", "Modified", "IsDeleted" },
                values: new object[] { "admin@admin.ru", "jos8YOiZy/6LLcFwnX2zCAWG4jyXtiEUTOUP0o0jpSL5da6A", 1, DateTime.UtcNow, DateTime.UtcNow, false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
