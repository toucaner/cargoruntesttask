﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Utils.Common;

namespace CargoRunTestTask.Bll.User
{
    public interface IUserService
    {
        #region Methods

        Task<Result<UserDto>> GetUser(long userId);
        
        Task<Result<UserDto>> FindUser(string email);
        
        Task<Result<UserDto>> FindUser(string email, string password);

        Task<Result> CreateUser(UserDto user);

        Task<Result<IEnumerable<UserDto>>> GetUserList();

        Task<Result<IEnumerable<UserDto>>> GetUserList(string filter, int page, int pageSize);

        Task<Result> UpdateUser(UserDto dto);

        Task<Result> DeleteUser(long userId);

        Task<Result<int>> GetUserCount(string filter, bool withDeleted = false);

        #endregion
    }
}