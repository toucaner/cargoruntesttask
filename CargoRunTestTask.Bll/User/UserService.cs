﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CargoRunTestTask.Bll.Base;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Dal.Repository.User;
using CargoRunTestTask.Dal.Repository.UserRole;
using CargoRunTestTask.Utils.Common;
using CargoRunTestTask.Utils.Enums;
using CargoRunTestTask.Utils.Helpers;
using Microsoft.Extensions.Logging;

namespace CargoRunTestTask.Bll.User
{
    public class UserService : RepositoryServiceBase<Core.Domain.User>, IUserService
    {
        #region Fields

        private readonly IUserRepository _userRepository;

        private readonly IUserRoleRepository _userRoleRepository;

        #endregion

        #region Constructor

        public UserService(IMapper mapper,
            IUserRepository repository,
            IUserRoleRepository userRoleRepository,
            ILogger<UserService> logger)
            : base(mapper, repository, logger)
        {
            _userRepository = repository;
            _userRoleRepository = userRoleRepository;
        }

        #endregion

        #region Methods

        public async Task<Result<UserDto>> GetUser(long userId)
        {
            try
            {
                var user = await _userRepository.Get(userId);
                var result = Mapper.Map<UserDto>(user);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<UserDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<UserDto>> FindUser(string email)
        {
            try
            {
                var user = await _userRepository.GetUser(email);
                var result = Mapper.Map<UserDto>(user);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<UserDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<UserDto>> FindUser(string email, string password)
        {
            try
            {
                var user = await _userRepository.GetUser(email);
                var isCorrect = CryptographyHelper.Verify(password, user.Password);
                return isCorrect
                    ? Result.Ok(Mapper.Map<UserDto>(user))
                    : Result.Fail<UserDto>(new Error(ErrorCode.UserNotFound));
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<UserDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result> CreateUser(UserDto dto)
        {
            try
            {
                var user = Mapper.Map<Core.Domain.User>(dto);
                user.Password = CryptographyHelper.Hash(user.Password);
                user.Role = await _userRoleRepository.Get(dto.RoleId);

                await Add(user);

                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<IEnumerable<UserDto>>> GetUserList(string filter, int page, int pageSize)
        {
            try
            {
                var users = await _userRepository.GetAll(filter, page, pageSize);
                var result =  Mapper.Map<IEnumerable<UserDto>>(users);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<IEnumerable<UserDto>>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result> UpdateUser(UserDto dto)
        {
            try
            {
                var user = await _userRepository.Get(dto.Id);
                user.Password = CryptographyHelper.Hash(user.Password);
                user.Role = await _userRoleRepository.Get(dto.RoleId);

                await Update(user);

                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<IEnumerable<UserDto>>> GetUserList()
        {
            try
            {
                var users = await _userRepository.GetAll();
                var result = Mapper.Map<IEnumerable<UserDto>>(users);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<IEnumerable<UserDto>>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result> DeleteUser(long userId)
        {
            try
            {
                await Delete(userId);
                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<int>> GetUserCount(string filter, bool withDeleted = false)
        {
            try
            {
                var result = await _userRepository.GetUserCount(filter, withDeleted);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<int>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        #endregion
    }
}