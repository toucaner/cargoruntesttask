﻿using CargoRunTestTask.Bll.User;
using CargoRunTestTask.Bll.UserRole;
using Microsoft.Extensions.DependencyInjection;

namespace CargoRunTestTask.Bll
{
    public static class Entry
    {
        #region Methods

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddScoped(typeof(IUserService), typeof(UserService))
                .AddScoped(typeof(IUserRoleService), typeof(UserRoleService));

            return services;
        }

        #endregion
    }
}