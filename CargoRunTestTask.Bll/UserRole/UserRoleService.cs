﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CargoRunTestTask.Bll.Base;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Dal.Repository.UserRole;
using CargoRunTestTask.Utils.Common;
using CargoRunTestTask.Utils.Enums;
using Microsoft.Extensions.Logging;

namespace CargoRunTestTask.Bll.UserRole
{
    public class UserRoleService : RepositoryServiceBase<Core.Domain.UserRole>, IUserRoleService
    {
        #region Constructor

        public UserRoleService(IMapper mapper,
            IUserRoleRepository repository,
            ILogger<UserRoleService> logger)
            : base(mapper, repository, logger)
        {
        }

        #endregion

        #region Methods

        public async Task<Result<IEnumerable<UserRoleDto>>> GetRoles()
        {
            try
            {
                var roles = await GetAll();
                var result = Mapper.Map<IEnumerable<UserRoleDto>>(roles);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<IEnumerable<UserRoleDto>>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        #endregion
    }
}