﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CargoRunTestTask.Core.Dto;
using CargoRunTestTask.Utils.Common;

namespace CargoRunTestTask.Bll.UserRole
{
    public interface IUserRoleService
    {
        #region Methods

        Task<Result<IEnumerable<UserRoleDto>>> GetRoles();

        #endregion
    }
}