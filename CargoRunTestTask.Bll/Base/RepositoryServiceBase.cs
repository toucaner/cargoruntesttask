﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CargoRunTestTask.Core.Domain;
using CargoRunTestTask.Dal.Repository;
using Microsoft.Extensions.Logging;

namespace CargoRunTestTask.Bll.Base
{
    public abstract class RepositoryServiceBase<TEntity> : ServiceBase where TEntity : EntityBase
    {
        #region Fields

        protected readonly IGenericRepository<TEntity> Repository;

        #endregion

        #region Constructor

        protected RepositoryServiceBase(IMapper mapper, IGenericRepository<TEntity> repository,
            ILogger<RepositoryServiceBase<TEntity>> logger) : base(mapper, logger)
        {
            Repository = repository;
        }

        #endregion

        #region Methods

        protected async Task<TEntity> Get(long id)
        {
            return await Repository.Get(id);
        }

        protected async Task<IEnumerable<TEntity>> GetAll(bool withDeleted = false)
        {
            return await Repository.GetAll(withDeleted);
        }

        protected async Task Add(TEntity entity)
        {
            await Repository.Insert(entity);
        }

        protected async Task Update(TEntity entity)
        {
            await Repository.Update(entity);
        }

        protected async Task Delete(long id)
        {
            await Repository.Delete(id);
        }

        protected async Task<int> Count(bool withDeleted = false)
        {
            return await Repository.Count(withDeleted);
        }

        #endregion
    }
}