﻿using AutoMapper;
using Microsoft.Extensions.Logging;

namespace CargoRunTestTask.Bll.Base
{
    public abstract class ServiceBase
    {
        #region Fields

        protected readonly IMapper Mapper;

        protected readonly ILogger<ServiceBase> Logger;

        #endregion

        #region Constructor

        protected ServiceBase(IMapper mapper, ILogger<ServiceBase> logger)
        {
            Mapper = mapper;
            Logger = logger;
        }

        #endregion
    }
}